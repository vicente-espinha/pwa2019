var mongodb = require("mongodb");

var lib = require("./lib");
var common = require("./common");

var rest = module.exports = {

    account: function (req, rep, session, query, payload) {
        switch (req.method) {

            case 'GET':
                common.accounts.findOne({
                    _id: common.sessions[session].accountNo
                }, {}, function (err, account) {
                    if (err) {
                        err.msg = 'No object in the database';
                        lib.sendJSON(rep, 400, 'ERROR', err);
                        return;
                    }
                    lib.sendJSON(rep, 200, 'OK', account);
                });
                break;

            case 'POST':
                common.accounts.findOne({
                    email: payload.recipient
                }, function (err, recipient) {
                    if (err || !recipient || recipient._id.equals(common.sessions[session].accountNo)) {
                        lib.sendJSON(rep, 400, 'ERROR', {
                            msg: 'Recipient invalid'
                        });
                        return;
                    }
                    if (!('amount' in payload) || isNaN(payload.amount) || payload.amount <= 0) {
                        lib.sendJSON(rep, 400, 'ERROR', {
                            msg: 'Amount of payload invalid'
                        });
                        return;
                    }
                    common.accounts.findOne({
                        _id: common.sessions[session].accountNo
                    }, {}, function (err, account) {
                        if (err) {
                            err.msg = 'No object in the database';
                            lib.sendJSON(rep, 400, 'ERROR', err);
                            return;
                        }
                        if (account.balance - payload.amount < account.limit) {
                            lib.sendJSON(rep, 400, 'ERROR', {
                                msg: 'Limit exceeded'
                            });
                            return;
                        }
                        common.accounts.findOneAndUpdate({
                            _id: common.sessions[session].accountNo
                        }, {
                            $set: {
                                balance: account.balance - payload.amount,
                                lastOperation: new Date()
                            }
                        }, {
                            returnOriginal: false
                        }, function (err, updated) {
                            if (err) {
                                err.msg = 'Update failed';
                                lib.sendJSON(rep, 400, 'ERROR', err);
                            }
                            common.accounts.findOneAndUpdate({
                                _id: recipient._id
                            }, {
                                $inc: {
                                    balance: payload.amount
                                }
                            }, {
                                returnOriginal: false
                            }, function (err, updated_r) {
                                if (!err) {
                                    // destination account balance increased
                                    var entry = {
                                        date: updated.value.lastOperation,
                                        amount: payload.amount,
                                        description: payload.description,
                                        balance_sender: updated.value.balance,
                                        balance_recipient: updated_r.value.balance,
                                        accountNo: common.sessions[session].accountNo,
                                        recipient_id: recipient._id
                                    };
                                    common.history.insertOne(entry);
                                    lib.sendJSON(rep, 200, 'OK', updated.value);
                                }
                            });
                        });
                    });
                });
                break;

            default:
                lib.sendJSON(rep, 400, 'Unhandled method', {
                    err: 'Unhandled method'
                });
        }
    },

    predefined: function (req, rep, session, query, payload) {
        switch (req.method) {
            case 'GET':
                common.predefined.find({
                    accountNo: common.sessions[session].accountNo
                }).sort({
                    name: 1
                }).toArray(function (err, docs) {
                    if (err) {
                        lib.sendJSON(rep, 400, 'Find failed', err);
                    } else {
                        lib.sendJSON(rep, 200, 'OK', docs);
                    }
                });
                break;

            case 'POST':
                payload.accountNo = common.sessions[session].accountNo;
                common.predefined.insertOne(payload, function (err, inserted) {
                    if (err) {
                        lib.sendJSON(rep, 400, 'Insert failed', err);
                    } else {
                        lib.sendJSON(rep, 200, 'OK', inserted.ops[0]);
                    }
                });
                break;

            case 'PUT':
                payload.accountNo = common.sessions[session].accountNo;
                common.predefined.findOneAndUpdate({
                    accountNo: common.sessions[session].accountNo,
                    _id: mongodb.ObjectId(query._id)
                }, {
                    $set: payload
                }, {
                    returnOriginal: false
                }, function (err, updated) {
                    if (err) {
                        lib.sendJSON(rep, 400, 'Update failed', err);
                    } else {
                        lib.sendJSON(rep, 200, 'OK', updated.value);
                    }
                });
                break;

            case 'DELETE':
                common.predefined.deleteOne({
                    accountNo: common.sessions[session].accountNo,
                    _id: mongodb.ObjectId(query._id)
                }, function (err, deleted) {
                    if (err) {
                        lib.sendJSON(rep, 400, 'Delete failed', err);
                    } else {
                        lib.sendJSON(rep, 200, 'OK', {
                            count: deleted.deletedCount
                        });
                    }
                });
                break;

            default:
                lib.sendJSON(rep, 400, 'Unhandled method', {
                    err: 'Unhandled method'
                });
        }
    },

    requests: function (req, rep, session, query, payload) {
        console.log(common.sessions[session].role);
        if(common.sessions[session].role !== "employee"){
            lib.sendJSON(rep, 401, 'Not authenticated', {
                err: 'Need to be employee to access it'
            });
            return;
        }
        switch (req.method) {
            case 'GET':
                if (Object.keys(query).length === 0) { //check if its empty --> get all the requests
                    common.requests.find({}).toArray(function (err, requests) {
                        if (err) {
                            err.msg = 'No access to requests';
                            lib.sendJSON(rep, 400, 'ERROR', err);
                            return;
                        }
                        lib.sendJSON(rep, 200, 'OK', requests);
                    });
                } else if (query.email !== undefined) { // search by email
                    common.requests.findOne({
                        email: query.email
                    }, {}, function (err, request) {
                        if (err) {
                            err.msg = 'No object in the database';
                            lib.sendJSON(rep, 400, 'ERROR', err);
                            return;
                        }
                        lib.sendJSON(rep, 200, 'OK', request);
                    });
                } else if (query.from !== undefined) { //search by status and text(email)
                    var from = parseInt(query.from);
                    var to = parseInt(query.to);
                    if (isNaN(from) || isNaN(to) || from <= 0 || to <= 0) {
                        var err = {
                            msg: 'From and to parameters set incorrectly'
                        };
                        lib.sendJSON(rep, 400, 'ERROR', err);
                        return;
                    }
                    var selection;
                    if (query.status == "") {
                        selection = {
                            email: {
                                $regex: query.search
                            }
                        }
                    } else {
                        selection = {
                            $and: [{
                                    status: query.status
                                },
                                {
                                    email: {
                                        $regex: query.search
                                    }
                                }
                            ]
                        }
                    }
                    common.requests.aggregate([{
                        $match: selection
                    }, {
                        $skip: from - 1
                    }, {
                        $limit: to
                    }]).toArray(function (err, requests) {
                        if (err) {
                            err.msg = 'No access to requests';
                            lib.sendJSON(rep, 400, 'ERROR', err);
                            return;
                        }
                        lib.sendJSON(rep, 200, 'OK', requests);
                    });
                }
                break;
            case 'POST':
                if (query.accepted == 'true') { //change to accepted
                    common.requests.updateOne({
                        email: payload.new_user.email
                    }, {
                        $set: {
                            status: "accepted"
                        },
                        $addToSet: {
                            log: payload.log
                        }
                    }, function (err, updated) {
                        if (err) {
                            lib.sendJSON(rep, 400, 'Change to accepted failed', err);
                        } else {
                            common.accounts.insertOne(payload.new_user, function (error, inserted) { //create account if does not exist already
                                if (error) {
                                    common.accounts.updateOne({ //account already exists , so set account to active
                                        email: payload.new_user.email
                                    }, {
                                        $set: {
                                            active: true
                                        },
                                    }, function (new_error, updated) {
                                        if (new_error) {
                                            lib.sendJSON(rep, 400, 'Change to update accepted failed', new_error);
                                        } else {
                                            lib.sendJSON(rep, 200, 'OK', updated);
                                        }
                                    });
                                } else {
                                    lib.sendJSON(rep, 200, 'OK', inserted.ops[0]);
                                }
                            })
                        }
                    })
                } else if (query.rejected == 'true') { //change to rejected
                    common.requests.updateOne({
                        email: payload.request.email
                    }, {
                        $set: {
                            status: "rejected"
                        },
                        $addToSet: {
                            log: payload.log
                        }
                    }, function (err, updated) {
                        if (err) {
                            lib.sendJSON(rep, 400, 'Change to rejected failed', err);
                        } else {
                            common.accounts.updateOne({   //set account to unactive
                                email: payload.request.email
                            }, {
                                $set: {
                                    active: false
                                },
                            }, function (error, new_updated) {
                                if (error) {
                                    lib.sendJSON(rep, 400, 'Disable user failed', error);
                                } else {
                                    lib.sendJSON(rep, 200, 'OK', new_updated);
                                }
                            })
                        }
                    })
                }

                break;

            case 'PUT':
                common.requests.insertOne(payload, function (err, inserted) { //insert new request
                    if (err) {
                        lib.sendJSON(rep, 400, 'Request failed', err);
                    } else {
                        lib.sendJSON(rep, 200, 'OK', inserted.ops[0]);
                    }
                });
                break;

            default:
                lib.sendJSON(rep, 400, 'Unhandled method', {
                    err: 'Unhandled method'
                });
        }
    },

    history: function (req, rep, session, query, payload) {
        var from = parseInt(query.from);
        var to = parseInt(query.to);
        if (isNaN(from) || isNaN(to) || from <= 0 || to <= 0) {
            var err = {
                msg: 'From and to parameters set incorrectly'
            };
            lib.sendJSON(rep, 400, 'ERROR', err);
            return;
        }
        var selection = {
            $or: [{
                    accountNo: common.sessions[session].accountNo
                },
                {
                    recipient_id: common.sessions[session].accountNo
                }
            ]
        };
        if (query.filter) {
            selection.description = {
                $regex: query.filter,
                $options: 'i'
            };
        }
        common.history.aggregate([{
                $match: selection
            },
            {
                $lookup: {
                    from: 'accounts',
                    localField: 'recipient_id',
                    foreignField: '_id',
                    as: 'recipient_data'
                }
            },
            {
                $unwind: {
                    path: '$recipient_data'
                }
            },
            {
                $lookup: {
                    from: 'accounts',
                    localField: 'accountNo',
                    foreignField: '_id',
                    as: 'sender_data'
                }
            },
            {
                $unwind: {
                    path: '$sender_data'
                }
            },
            {
                $addFields: {
                    recipient_email: '$recipient_data.email'
                }
            },
            {
                $addFields: {
                    sender_email: '$sender_data.email'
                }
            },
            {
                $addFields: {
                    balance: {
                        $cond: {
                            if: {
                                $eq: ['$sender_email', common.sessions[session].login]
                            },
                            then: '$balance_sender',
                            else: '$balance_recipient'
                        }
                    }
                }
            },
            {
                $project: {
                    recipient_data: false,
                    sender_data: false,
                    balance_sender: false,
                    balance_recipient: false
                }
            },
            {
                $sort: {
                    date: -1
                }
            },
            {
                $skip: from - 1
            },
            {
                $limit: to
            }
        ]).toArray(function (err, docs) {
            if (err) {
                err.msg = 'No access to history';
                lib.sendJSON(rep, 400, 'ERROR', err);
                return;
            }
            lib.sendJSON(rep, 200, 'OK', docs);
        });
    },

    recipients: function (req, rep, session, query, payload) {
        common.history.aggregate([{
                $match: {
                    accountNo: common.sessions[session].accountNo
                }
            },
            {
                $group: {
                    _id: '$recipient_id'
                }
            },
            {
                $lookup: {
                    from: 'accounts',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'recipient_data'
                }
            },
            {
                $unwind: {
                    path: '$recipient_data'
                }
            },
            {
                $addFields: {
                    email: '$recipient_data.email'
                }
            },
            {
                $project: {
                    _id: false,
                    recipient_data: false
                }
            },
            {
                $sort: {
                    email: 1
                }
            }
        ]).toArray(function (err, docs) {
            if (err) {
                err.msg = 'No access to history';
                lib.sendJSON(rep, 400, 'ERROR', err);
                return;
            }
            lib.sendJSON(rep, 200, 'OK', docs);
        });
    },

    login: function (req, rep, session, query, payload) {
        switch (req.method) {

            case 'GET':
                lib.sendJSON(rep, 200, 'OK', common.sessions[session]);
                break;

            case 'POST':
                if (!payload.email || !payload.password) {
                    delete common.sessions[session].login;
                    lib.sendJSON(rep, 401, 'Not authenticated', {
                        err: 'Broken credentials'
                    });
                    return;
                }
                common.accounts.findOne(payload, {}, function (err, account) {
                    if (err || !account || !account.active) {
                        delete common.sessions[session].login;
                        lib.sendJSON(rep, 401, 'Not authenticated', {
                            err: 'Bad password'
                        });
                        return;
                    }
                    common.sessions[session].login = payload.email;
                    common.sessions[session].accountNo = account._id;
                    common.sessions[session].role = account.type;
                    lib.sendJSON(rep, 200, 'OK', account);
                });
                break;

            case 'PUT':
                if (!payload.password) {
                    lib.sendJSON(rep, 401, 'Error', {
                        err: 'Invalid new password'
                    });
                    return;
                }
                common.accounts.findOneAndUpdate({
                    _id: mongodb.ObjectId(common.sessions[session].accountNo)
                }, {
                    $set: {
                        password: payload.password
                    }
                });
                lib.sendJSON(rep, 200, 'OK', payload);
                break;

            case 'DELETE':
                delete common.sessions[session].login;
                lib.sendJSON(rep, 200, 'OK', {
                    msg: 'Logged out'
                });
                break;

            default:
                lib.sendJSON(rep, 400, 'Unhandled method', {
                    err: 'Unhandled method'
                });
        }
    }

};