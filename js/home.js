app1.controller("Home", ['$http', 'common', function ($http, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.message = {};

    ctrl.creds = {
        email: "aaa@aaa.com",
        password: "a"
    };
    ctrl.creds_request = {
        email: "cenas@aaa.com",
        password: "cenas",
        status: "pending",
        log:[]
    };

    ctrl.doLogin = function () {
        $http.post('/login', ctrl.creds).then(
            function (rep) {
                ctrl.session.login = rep.data.email;
                common.session.role = rep.data.type;
                console.log(common.session)
                common.rebuildMenu(rep.data.type);
                ctrl.message = {
                    ok: 'Login successful'
                };
            },
            function (err) {
                ctrl.message = {
                    error: 'Login failed'
                };
            }
        );
    }

    ctrl.doLogout = function () {
        $http.delete('/login').then(
            function (rep) {
                ctrl.session.login = null;
                common.rebuildMenu();
                ctrl.message = {
                    ok: 'Logout successful'
                };
            }
        );
    }

    //function that handles requests
    ctrl.doRequest = function () {

        // sends request to db
        $http.put('/requests', ctrl.creds_request).then(
            function (rep) { //success
                swal({
                    title: 'Success',
                    text: 'Request done successfully!',
                    type: 'success',
                    confirmButtonText: 'Close'
                })
            },
            function (err) { //error
                if (err.data.errmsg.includes("duplicate key")) { //if request has already been made
                    $http.get('/requests?&email=' + ctrl.creds_request.email).then(
                        function (rep) {
                            switch (rep.data.status) { //request status
                                case "pending":
                                    swal({
                                        title: 'Pending!',
                                        text: 'You have already made a request and it\'s still pending!',
                                        type: 'warning',
                                        confirmButtonText: 'Close'
                                    })
                                    break;
                                case "accepted":
                                    swal({
                                        title: 'Accepted!',
                                        text: 'You have already made a request and have been accepted!',
                                        type: 'success',
                                        confirmButtonText: 'Close'
                                    })
                                    break;
                                case "rejected":
                                    swal({
                                        title: 'Rejected!',
                                        text: 'You have already made a request and has been rejected!',
                                        type: 'error',
                                        confirmButtonText: 'Close'
                                    })
                                    break;
                            }
                        },

                        function (err) { //if it was not able to get any request with that email
                            swal({
                                title: 'Error!',
                                text: 'Something went wrong connecting to the server!!',
                                type: 'error',
                                confirmButtonText: 'Close'
                            })
                        }
                    );
                } else { //if it was not possible to introduce this new request in the db
                    swal({
                        title: 'Error!',
                        text: 'Something went wrong! \n Check if you introduced a valid email!',
                        type: 'error',
                        confirmButtonText: 'Close'
                    })
                }
            }
        );
    }

}]);