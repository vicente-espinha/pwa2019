app1.controller("ModalTransfer", ["$http","$uibModalInstance", "recipients", function($http,$uibModalInstance, recipients) {
    var ctrl = this;
    ctrl.account = {};
    ctrl.recipients = recipients;
    ctrl.transfer = { recipient: '', amount: 1, description: '' };
    ctrl.submit = function() {
        $http.post('/account', ctrl.transfer).then(
            function(rep) { ctrl.account = rep.data; refreshRecipients(); zeroData(); ctrl.message = { ok: 'Transfer successful' }; },
            function(err) { ctrl.message = { error: 'Transfer failed' }; }
        );
    }


    $http.get('/account').then(
        function(rep) { ctrl.account = rep.data; },
        function(err) { ctrl.account = {}; }
    );


    ctrl.transferInvalid = function() {
        return isNaN(ctrl.transfer.amount) || ctrl.transfer.amount <= 0 || ctrl.account.balance - ctrl.transfer.amount < ctrl.account.limit;
    }

    ctrl.close = function(){
        $uibModalInstance.close(ctrl.transfer);
    }

    var refreshRecipients = function() {
        $http.get('/recipients').then(
            function(rep) { ctrl.recipients = rep.data; },
            function(err) { ctrl.recipients = []; }
        );
    }

    var zeroData = function() {
        ctrl.transfer = { recipient: '', amount: 0, description: '' };
        ctrl.message = {};
    }
}]);