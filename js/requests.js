app1.controller("Requests", ['$http', 'common', function ($http, common) {
    var ctrl = this;
    ctrl.session = common.session;
    ctrl.message = {};
    ctrl.state_selected = "";
    ctrl.state = ['pending', 'accepted', 'rejected'];
    ctrl.last10 = true;
    ctrl.filter = '';
    ctrl.requests = [];

    //shows all the requests
    ctrl.refreshRequests = function () {
        var to = ctrl.last10 ? 5 : 999999;
        $http.get('/requests?&from=1&to=' + to + '&status=' + ctrl.state_selected + '&search=' + ctrl.filter).then(
            function (rep) {
                ctrl.requests = rep.data;
            },
            function (err) {
                swal({
                    title: 'Error!',
                    text: 'Access denied!!',
                    type: 'error',
                    confirmButtonText: 'Close'
                })

                ctrl.message = 'Error retrieving history';
            }
        );


    };

    // accepts the request
    ctrl.acceptRequest = function (request) {
        let new_user = {
            email: request.email,
            password: request.password,
            balance: 1000,
            limit: -1000,
            type: "customer",
            active: true,
            lastOperation: new Date()
        };

        let payload = {
            new_user: new_user,
            log: {
                from: request.status,
                to: "accepted",
                date: new Date()
            }
        }
        $http.post('/requests?&accepted=true', payload).then(
            function (rep) {
                swal({
                    title: 'Success',
                    text: 'Request accepted successfully!',
                    type: 'success',
                    confirmButtonText: 'Close'
                });
                ctrl.refreshRequests();
            },
            function (err) {
                swal({
                    title: 'Error!',
                    text: 'Something went wrong connecting to the server!!',
                    type: 'error',
                    confirmButtonText: 'Close'
                })
            }
        );

    };

    // rejects the request 
    ctrl.rejectRequest = function (request) {

        let payload = {
            request: request,
            log: {
                from: request.status,
                to: "rejected",
                date: new Date()
            }
        }

        $http.post('/requests?&rejected=true', payload).then(
            function (rep) {
                swal({
                    title: 'Success',
                    text: 'Request rejected successfully!',
                    type: 'success',
                    confirmButtonText: 'Close'
                });
                ctrl.refreshRequests();
            },
            function (err) {
                swal({
                    title: 'Error!',
                    text: 'Something went wrong connecting to the server!!',
                    type: 'error',
                    confirmButtonText: 'Close'
                })
            }
        );
    };

    ctrl.refreshRequests();
}]);